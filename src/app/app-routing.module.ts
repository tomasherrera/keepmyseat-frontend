import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CompaniesComponent } from './companies/companies.component'
import { TurnsComponent } from './turns/turns.component'

const routes: Routes = [
  { path: '', redirectTo: '/companies', pathMatch: 'full'},
  { path: 'companies', component: CompaniesComponent },
  { path: 'turns', component: TurnsComponent },
]

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}