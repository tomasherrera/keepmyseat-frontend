import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response} from '@angular/http';

import { Company } from './company.model';
import { CompanyUser } from './company_user.model';

@Injectable()
export class CompaniesService {

  public companies: Company[];
  public subscribed_companies: Company[];
  public company_users: CompanyUser[];
  public company_user: CompanyUser;

  constructor(private http: Http) { }

  getCompanies(){
    return this.http.get('http://localhost:3000/companies').subscribe(
      (response: Response) => {
        this.setCompanies(response.json());
     });
  }

  getSubscribedCompanies(){
    return this.http.get('http://localhost:3000/subscribed_companies').subscribe(
      (response: Response) => {
        this.subscribed_companies = response.json();
     });
  }

  getSubscriptions(){
    return this.http.get('http://localhost:3000/company_users').subscribe(
      (response: Response) => {
        this.setSubscriptions(response.json());
     });
  }

  setCompanies(companies: Company[]){
    this.companies = companies;
  }

  setSubscriptions(company_users: CompanyUser[]){
    this.company_users = company_users;
  }

  companySubscribed(company: Company){
    var subscribed = false;
    if(this.company_users.filter(company_user => company_user.company_id == company.id).length > 0) {
      subscribed = true;
    }
    return subscribed;
  }

  subscribeToCompany(company_id){
    this.company_user = new CompanyUser;
    this.company_user.company_id = company_id;
    return this.http.post('http://localhost:3000/company_users', this.company_user).subscribe(
      (response: Response) => {
        this.getCompanies();
        this.getSubscriptions();
     });
  }
}
