import { Component, OnInit } from '@angular/core';
import { CompaniesService } from '../companies/companies.service';
import { TurnsService } from '../turns/turns.service';
import { Ng2Cable, Broadcaster } from 'ng2-cable';

@Component({
  selector: 'app-turns',
  templateUrl: './turns.component.html',
  styleUrls: ['./turns.component.css']
})
export class TurnsComponent implements OnInit {

  public notification: string = "";

  constructor(private companiesService: CompaniesService, private turnsService: TurnsService, private ng2cable: Ng2Cable,
              private broadcaster: Broadcaster) { }

  ngOnInit() {
    this.companiesService.getSubscribedCompanies();
    this.ng2cable.subscribe('http://localhost:3000/cable', 'ActivityChannel', { room: 'My room' });
    this.broadcaster.on<string>('ActivityChannel').subscribe(
      notification => {
        this.notification = notification;
        this.companiesService.getSubscribedCompanies();
      }
    );
  }

  getCallingClass(){
    return {
      "text-white": true,
      "bg-success" :true,
      "mb-3": true
    }
  }

}
