import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response} from '@angular/http';

import { Company } from '../companies/company.model';
import { Turn } from './turn.model';

import { CompaniesService } from '../companies/companies.service';

@Injectable()
export class TurnsService {

  constructor(private http: Http, private companiesService: CompaniesService) { }

  public number: number;

  getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  requestTurn(company_id){
    var turn = new Turn;
    turn.company_id = company_id;
    turn.turn_number = this.getRandomInt(100, 1000);
    return this.http.post('http://localhost:3000/turns', turn).subscribe(
      (response: Response) => {
        this.companiesService.getSubscribedCompanies();
    });
  }
}
