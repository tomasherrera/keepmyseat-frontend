export class Turn{
  public category_id: number;
  public company_id: number;
  public user_id: number;
  public turn_number: number;
  public codeletter: string;
  public status: string;
}