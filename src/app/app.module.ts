import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NavigationbarComponent } from './navigationbar/navigationbar.component';
import { DashboardComponent } from './dashboard/dashboard.component';

import { CompaniesService } from './companies/companies.service';
import { TurnsService } from './turns/turns.service'
import { CompaniesComponent } from './companies/companies.component';
import { TurnsComponent } from './turns/turns.component';

import { AppRoutingModule} from './app-routing.module';
import { RouterModule } from '@angular/router';
import { Ng2CableModule } from 'ng2-cable';

@NgModule({
  declarations: [
    AppComponent,
    NavigationbarComponent,
    DashboardComponent,
    CompaniesComponent,
    TurnsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    AppRoutingModule,
    Ng2CableModule
  ],
  providers: [ CompaniesService, TurnsService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
